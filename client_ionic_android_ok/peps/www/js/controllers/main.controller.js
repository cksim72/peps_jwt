angular.module('pepsApp')
    .controller('MainCtrl', function($scope, $state, $ionicPopup, AuthService, AUTH_EVENTS) {
        $scope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
        AuthService.logout();
        $state.go('login');
        var alertPopup = $ionicPopup.alert({
            title: 'Credentials not found!',
            template: 'Please check NRIC(ID) and password'
        });
    });
});
