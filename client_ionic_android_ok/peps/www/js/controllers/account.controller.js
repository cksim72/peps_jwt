(function() {

    angular.module('pepsApp')
        .controller('AccountCtrl', AccountCtrl);

        AccountCtrl.$inject = ["$window", "$ionicHistory", "$ionicLoading", "$q", "$ionicPopup", "$ionicModal", "$scope", 
            "$stateParams", "$state", "$rootScope", "$timeout", "AuthService", "NursesAPI"];


        function AccountCtrl($window, $ionicHistory, $ionicLoading, q, $ionicPopup, $ionicModal, $scope, 
            $stateParams, $state, $rootScope, $timeout, AuthService, NursesAPI) {
            
            vm = this;

            vm.editmode=0; // View = 0, Edit = 1

            vm.user={};
            vm.modalUser = {};

            vm.status = {
                message : "",
                code : 0
            };

            NursesAPI.search($rootScope.loggedUser.id)
                .then(function(data) {

                    vm.user=data;
                    vm.user.birth_date = new Date(vm.user.birth_date);

                    vm.urlphoto = "https://s3-ap-southeast-1.amazonaws.com/nus-iss-stackup/nurses/"+vm.user.id+".jpg";

                    console.log("vm.user: ", vm.user);
                })
                .catch(function(err) {

                    var alertPopup = $ionicPopup.alert({
                        title: 'Error retrieving from database!',
                        template: 'Logging Off now'
                    });

                    $state.go('login');
                    
                })

            vm.editprofile = function() {
                console.log("Edit Profile");
                vm.editmode=1;
             
            }

            vm.saveprofile = function() {
                console.log("Save Profile");
                vm.editmode=0;

                console.log("vm.user: ", vm.user);

                NursesAPI.save(vm.user)
                .then(function(data) {

                    var alertPopup = $ionicPopup.alert({
                    title: 'Profile updated successfully !',
                });
                })
                .catch(function(err) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Error updating profile!',
                    });
                })
            }

            vm.cancel = function() {
                console.log("Cancel");
                vm.editmode=0;
            }

            vm.logout = function() {
                console.log("Logout complete");

                AuthService.logout();

                $window.localStorage.clear();

                $ionicHistory.clearCache()
                    .then(function(){ 
                        $ionicHistory.clearHistory();
                        $state.go('login', $stateParams, {reload: true, inherit: false});
                    })
                // $state.go('login', {}, {reload: true, notify: false});
             }


            $ionicModal.fromTemplateUrl('templates/modal_photo.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
            });

            vm.showModal = function() {
                $scope.modal.show();
                vm.urlphoto = "https://s3-ap-southeast-1.amazonaws.com/nus-iss-stackup/nurses/placeholder.png";
            };
            
            vm.updatePhoto = function(photo) {        
                console.log("Modal inputs, photo: ", photo);

                // UploadS3API.uploadNurse(vm.user.id, photo)
                //     .then(function(data) {
                //         console.log("AcctCtrl, upload response: ", data);

                //         $ionicLoading.show({
                //             template: '<ion-spinner icon="android"></ion-spinner>'
                //         });

                //         $timeout(function(){
                //             $ionicLoading.hide();
                //             vm.urlphoto = "https://s3-ap-southeast-1.amazonaws.com/nus-iss-stackup/nurses/"+vm.user.id+".jpg";
                //             vm.status.message = "Successful";
                //             vm.status.code = 202;
                //         }, 2500);

                //     })
                //     .catch(function(err) {
                //         console.log("AcctCtrl, upload err: ", err);
                //          vm.status.message = "Error";
                //          vm.status.code = 500;
                //     })
            };

            vm.closeModal = function() {
                vm.urlphoto = "https://s3-ap-southeast-1.amazonaws.com/nus-iss-stackup/nurses/"+vm.user.id+".jpg";
                $scope.modal.hide();

            };

            vm.cancelModal = function() {
                vm.urlphoto = "https://s3-ap-southeast-1.amazonaws.com/nus-iss-stackup/nurses/"+vm.user.id+".jpg";
                $scope.modal.hide();
            };
            
        }

})();
