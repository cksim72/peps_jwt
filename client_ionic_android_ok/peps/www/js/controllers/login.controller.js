(function(){
	angular
		.module("pepsApp")
		.controller("LoginCtrl", LoginCtrl);

		LoginCtrl.$inject = ["$window", "$ionicHistory", "$http", "$state", "$rootScope", "$scope", "AuthService", "NursesAPI"];


		function LoginCtrl($window, $ionicHistory, $http, $state, $rootScope, $scope, AuthService, NursesAPI){
			var vm = this;

            vm.user = {};

            // for (var prop in $rootScope) {
            //     if (prop.substring(0,1) !== '$') {
            //         delete $rootScope[prop];
            //     }
            // }

            $rootScope.loggedUser = {};

            console.log("Logout complete");

            AuthService.logout();

            $window.localStorage.clear();

            $ionicHistory.clearCache()
                .then(function(){ 
                    $ionicHistory.clearHistory();

                })

            vm.login = function() {
                console.log("Login User: ", vm.user);

                $rootScope.loggedUser = {id: "", role: ""};

                AuthService.login(vm.user)
                    .then(function(msg) {
                        console.log("Login OK - msg: ",msg);
                        // NursesAPI.search(vm.user.id).then(function(data) {
                        //     $rootScope.loggedUser = data;
                        //     console.log("Login success - user: ", $rootScope.loggedUser);
                        //     $state.go("tab.patientlist");
                        // })

                        $rootScope.loggedUser.id=vm.user.id;
                        $rootScope.loggedUser.role=msg.role;    // Get permissions

                        $rootScope.supervisormode=false;
                        if (msg.role=="SUPERVISOR") $rootScope.supervisormode=true;

                        $state.go("tab.patientlist");  
                        
                    }, function(errMsg) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Login failed!',
                            template: errMsg
                    });
                });
            }

            vm.cancel = function() {
                console.log("User: ", vm.user);
            }

	}
})();
