(function() {

    angular.module('pepsApp')
        .controller('NurseCtrl', NurseCtrl)
        .controller('NurseListCtrl', NurseListCtrl);

        NurseCtrl.$inject = ["$state", "$scope", "$stateParams", "$ionicPopup", "NursesAPI"];
        NurseListCtrl.$inject = ["$state", "$rootScope", "$scope", "$ionicScrollDelegate", "filterFilter", "$location", "$anchorScroll", "$state", "$ionicHistory", "$ionicPopup", "NursesAPI"];

        function NurseCtrl($state, $scope, $stateParams, $ionicPopup, NursesAPI) {

            var vm = this;

            vm.user = {};

            console.log("$stateParams: ", $stateParams);

            vm.editmode=0;

            NursesAPI.search($stateParams.nurseId)
                .then(function(data) {
                    vm.user = data;
                    vm.user.birth_date = new Date(vm.user.birth_date);
                    console.log("NurseRecord: ", vm.user);
                })
                .catch(function(err) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Err PS1: Database Error!',
                        template: 'Logging Off now'
                    });
                    $state.go('login');
                })

            
            vm.editprofile = function() {
                console.log("Edit Profile");
                vm.editmode=1;
             
            }

            vm.saveprofile = function() {
                console.log("Save Profile");
                vm.editmode=0;

                console.log("vm.user: ", vm.user);

                NursesAPI.save(vm.user)
                .then(function(data) {

                    var alertPopup = $ionicPopup.alert({
                        title: 'Profile updated successfully !',
                    });
                })
                .catch(function(err) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Error updating profile!',
                    });
                })
            }

            vm.cancel = function() {
                console.log("Cancel");
                vm.editmode=0;
            }

        }

        function NurseListCtrl($state, $rootScope, $scope, $ionicScrollDelegate, filterFilter, $location, 
                    $anchorScroll, $state, $ionicHistory, $ionicPopup, NursesAPI) {

            var vm = this;
            
            vm.search = "";

            vm.nurseList = [];
  
            vm.supervisoraccess = ($rootScope.loggedUser.role == "SUPERVISOR");

            if (vm.supervisoraccess) {
                NursesAPI.list()
                .then(function(data) {
                    vm.nurseList = data;
                    console.log("Nurse List (length): ", vm.nurseList.length);
                })
                .catch(function(err) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Err N1: Database Error!',
                        template: 'Logging Off now'
                    });
                    $state.go('login');
                })
            }

            vm.scrollTop = function() {
                $ionicScrollDelegate.scrollTop();
            };
  
            vm.scrollBottom = function() {
                $ionicScrollDelegate.scrollBottom();
            };

            vm.clearSearch = function() {
                vm.search = '';
            };

            vm.viewnurse = function(id) {

                console.log("view nurse clicked");

                $state.go("tab.nursesingle", {nurseId: id});
            }

        }

})();