exports.epillbox_index = ['S1234567Z', 'S1974932A', 'S2091182C', 'S2191182C', 'S2291182C'];

exports.epillbox_table = [
                {   'id': 'S1234567Z',  
                    'box1_pill_name': 'Sitagliptin',            'box1_freq': '4', 'box1_dose': '1', 'box1_adhere': 'NG', 'box1_admin': 'S1234567Z',
                    'box2_pill_name': 'Pregabalin',             'box2_freq': '3', 'box2_dose': '1', 'box2_adhere': 'NG', 'box2_admin': 'S1234567Z',
                    'box3_pill_name': 'Insulin glargine',       'box3_freq': '3', 'box3_dose': '1', 'box3_adhere': 'NG', 'box3_admin': 'S1234567Z',
                    'box4_pill_name': 'Rosuvastatin calcium',   'box4_freq': '2', 'box4_dose': '2', 'box4_adhere': 'GD', 'box4_admin': 'S1234567Z',
                    'box5_pill_name': 'Tadalafil',              'box5_freq': '2', 'box5_dose': '1', 'box5_adhere': 'GD', 'box5_admin': 'S1234567Z',
                    'box6_pill_name': 'Colesevelam Hcl',        'box6_freq': '2', 'box6_dose': '2', 'box6_adhere': 'NG', 'box6_admin': 'S1234567Z',
                    'box7_pill_name': 'Carvedilol',             'box7_freq': '3', 'box7_dose': '1', 'box7_adhere': 'NG', 'box7_admin': 'S1234567Z',
                    'box8_pill_name': 'Estradiol',              'box8_freq': '3', 'box8_dose': '1', 'box8_adhere': 'GD', 'box8_admin': 'S1234567Z'

                },
            
                {   'id': 'S1974932A',  
                    'box1_pill_name': 'Carvedilol',             'box1_freq': '3', 'box1_dose': '1', 'box1_adhere': 'GD', 'box1_admin': 'S1974932A',
                    'box2_pill_name': 'Colesevelam Hcl',        'box2_freq': '2', 'box2_dose': '2', 'box2_adhere': 'GD', 'box2_admin': 'S1974932A',
                    'box3_pill_name': 'Tadalafil',              'box3_freq': '3', 'box3_dose': '2', 'box3_adhere': 'GD', 'box3_admin': 'S1974932A',
                    'box4_pill_name': 'Rosuvastatin calcium',   'box4_freq': '4', 'box4_dose': '1', 'box4_adhere': 'GD', 'box4_admin': 'S1974932A',
                    'box5_pill_name': 'Insulin glargine',       'box5_freq': '3', 'box5_dose': '1', 'box5_adhere': 'GD', 'box5_admin': 'S1974932A',
                    'box6_pill_name': 'Pregabalin',             'box6_freq': '2', 'box6_dose': '1', 'box6_adhere': 'GD', 'box6_admin': 'S1974932A',
                    'box7_pill_name': 'Estradiol',              'box7_freq': '1', 'box7_dose': '2', 'box7_adhere': 'GD', 'box7_admin': 'S1974932A',
                    'box8_pill_name': 'Sitagliptin',            'box8_freq': '2', 'box8_dose': '1', 'box8_adhere': 'GD', 'box8_admin': 'S1974932A'

                },

                {   'id': 'S2091182C',  
                    'box1_pill_name': 'Rosuvastatin calcium',   'box1_freq': '2', 'box1_dose': '1', 'box1_adhere': 'GD', 'box1_admin': 'S2091182C',
                    'box1_pill_name': 'Insulin glargine',       'box1_freq': '3', 'box1_dose': '1', 'box1_adhere': 'GD', 'box1_admin': 'S2091182C',
                    'box2_pill_name': 'Pregabalin',             'box2_freq': '4', 'box2_dose': '1', 'box2_adhere': 'GD', 'box2_admin': 'S2091182C',
                    'box3_pill_name': 'Estradiol',              'box3_freq': '1', 'box3_dose': '2', 'box3_adhere': 'GD', 'box3_admin': 'S2091182C',
                    'box4_pill_name': 'Sitagliptin',            'box4_freq': '3', 'box4_dose': '1', 'box4_adhere': 'GD', 'box4_admin': 'S2091182C',
                    'box5_pill_name': 'Carvedilol',             'box5_freq': '3', 'box5_dose': '1', 'box5_adhere': 'GD', 'box5_admin': 'S2091182C',
                    'box6_pill_name': 'Colesevelam Hcl',        'box6_freq': '3', 'box6_dose': '2', 'box6_adhere': 'GD', 'box6_admin': 'S2091182C',
                    'box7_pill_name': 'Tadalafil',              'box7_freq': '4', 'box7_dose': '1', 'box7_adhere': 'GD', 'box7_admin': 'S2091182C',
                    'box8_pill_name': 'Rosuvastatin calcium',   'box8_freq': '2', 'box8_dose': '2', 'box8_adhere': 'GD', 'box8_admin': 'S2091182C'

                },

                {   'id': 'S2191182C',  
                    'box1_pill_name': 'Rosuvastatin calcium',   'box1_freq': '3', 'box1_dose': '1', 'box1_adhere': 'GD', 'box1_admin': 'S2191182C',
                    'box2_pill_name': 'Estradiol',              'box2_freq': '2', 'box2_dose': '2', 'box2_adhere': 'GD', 'box2_admin': 'S2191182C',
                    'box3_pill_name': 'Pregabalin',             'box3_freq': '4', 'box3_dose': '1', 'box3_adhere': 'GD', 'box3_admin': 'S2191182C',
                    'box4_pill_name': 'Carvedilol',             'box4_freq': '1', 'box4_dose': '2', 'box4_adhere': 'GD', 'box4_admin': 'S2191182C',
                    'box5_pill_name': 'Sitagliptin',            'box5_freq': '2', 'box5_dose': '1', 'box5_adhere': 'GD', 'box5_admin': 'S2191182C',
                    'box6_pill_name': 'Insulin glargine',       'box6_freq': '3', 'box6_dose': '1', 'box6_adhere': 'GD', 'box6_admin': 'S2191182C',
                    'box7_pill_name': 'Rosuvastatin calcium',   'box7_freq': '4', 'box7_dose': '1', 'box7_adhere': 'GD', 'box7_admin': 'S2191182C',
                    'box8_pill_name': 'Tadalafil',              'box8_freq': '3', 'box8_dose': '2', 'box8_adhere': 'GD', 'box8_admin': 'S2191182C'

                },

                {   'id': 'S2291182C',  
                    'box1_pill_name': 'Sitagliptin',            'box1_freq': '3', 'box1_dose': '1', 'box1_adhere': 'GD', 'box1_admin': 'S2291182C',
                    'box2_pill_name': 'Insulin glargine',       'box2_freq': '4', 'box2_dose': '2', 'box2_adhere': 'GD', 'box2_admin': 'S2291182C',
                    'box3_pill_name': 'Rosuvastatin calcium',   'box3_freq': '2', 'box3_dose': '2', 'box3_adhere': 'NG', 'box3_admin': 'S2291182C',
                    'box4_pill_name': 'Tadalafil',              'box4_freq': '3', 'box4_dose': '1', 'box4_adhere': 'NG', 'box4_admin': 'S2291182C',
                    'box5_pill_name': 'Rosuvastatin calcium',   'box5_freq': '1', 'box5_dose': '2', 'box5_adhere': 'GD', 'box5_admin': 'S2291182C',
                    'box6_pill_name': 'Estradiol',              'box6_freq': '2', 'box6_dose': '1', 'box6_adhere': 'GD', 'box6_admin': 'S2291182C',
                    'box7_pill_name': 'Pregabalin',             'box7_freq': '3', 'box7_dose': '1', 'box7_adhere': 'NG', 'box7_admin': 'S2291182C',
                    'box8_pill_name': 'Carvedilol',             'box8_freq': '4', 'box8_dose': '1', 'box8_adhere': 'GD', 'box8_admin': 'S2291182C'

                }   
            ];



            // ePillbox = require('./epillbox_table');

            // console.log("epillbox_index", ePillbox.epillbox_index);
            // console.log("epillbox_table", ePillbox.epillbox_table);

