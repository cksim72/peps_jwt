(function() {
    'use strict';

    angular.module('pepsApp')
        .controller('PatientCtrl', PatientCtrl)
        .controller('PatientListCtrl', PatientListCtrl);

        PatientCtrl.$inject = ["$rootScope", "$state", "$scope", "$stateParams", "$ionicPopup", "NOKsAPI", "$ionicModal", "PatientsAPI"];
        PatientListCtrl.$inject = ["$rootScope", "$scope", "$ionicScrollDelegate", "filterFilter", "$location", "$anchorScroll", "$state", "$ionicHistory", "$ionicPopup", "PatientsAPI"];

        function PatientCtrl($rootScope, $state, $scope, $stateParams, $ionicPopup, NOKsAPI, $ionicModal, PatientsAPI) {

            var vm = this;

            vm.user = {};

            console.log("$stateParams: ", $stateParams);

            vm.editmode=0;
            vm.showRegimen = false;
            vm.showNOK = false;

            vm.modalpillbox = {};
            vm.modalpillboxNum = 0;

            PatientsAPI.search($stateParams.patientId)
                .then(function(data) {
                    vm.user = data;
                    vm.user.birth_date = new Date(vm.user.birth_date);
                    console.log("Patient Record: ", vm.user);

                    vm.user_pillbox=vm.epillbox_table[vm.epillbox_index.indexOf(vm.user.id)];
                    console.log("user_pillbox: ", vm.user_pillbox);
                })
                .catch(function(err) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Err PS1: Database Error!',
                        template: 'Logging Off now'
                    });
                    $state.go('login');
                })

            
            vm.editprofile = function() {
                console.log("Edit Profile");
                vm.editmode=1;
             
            }

            vm.saveprofile = function() {
                console.log("Save Profile");
                vm.editmode=0;

                console.log("vm.user: ", vm.user);

                PatientsAPI.save(vm.user)
                .then(function(data) {

                    var alertPopup = $ionicPopup.alert({
                        title: 'Profile updated successfully !',
                    });
                })
                .catch(function(err) {
                    var alertPopup = $ionicPopup.alert({
                        title: 'Error updating profile!',
                    });
                })
            }

            vm.cancel = function() {
                console.log("Cancel");
                vm.editmode=0;
            }

            vm.toggleRegimen = function() {
                if (vm.showRegimen == true) 
                    vm.showRegimen = !vm.showRegimen;
                else
                    vm.showRegimen = true;

                vm.showNOK = false;

            }

            vm.toggleNOK = function() {

                vm.showRegimen = false;

                if (vm.showNOK == true) 
                    vm.showNOK = !vm.showNOK;
                else {

                    vm.nokList = [];

                    vm.showNOK = true;
                    
                    NOKsAPI.searchByPatientID(vm.user.id)
                    .then(function(data) {
                        vm.nokList = data;
                        console.log("Get All NOKs by PatientID: ", vm.nokList);
                    })
                    .catch(function(err) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Err NP1: Database Error!',
                            template: 'Logging Off now'
                        });

                        $state.go('login');
                    })

                }              

            }

            vm.pillBoxClick = function(num) {
                console.log("PillBox", num, " clicked");

                vm.modalpillbox = {};
                vm.modalpillboxNum = num;

                switch (num)
                {
                    case 1: vm.modalpillbox = Object.create(vm.user_pillbox.box1); break;
                    case 2: vm.modalpillbox = Object.create(vm.user_pillbox.box2); break;
                    case 3: vm.modalpillbox = Object.create(vm.user_pillbox.box3); break;
                    case 4: vm.modalpillbox = Object.create(vm.user_pillbox.box4); break;
                    case 5: vm.modalpillbox = Object.create(vm.user_pillbox.box5); break;
                    case 6: vm.modalpillbox = Object.create(vm.user_pillbox.box6); break;
                    case 7: vm.modalpillbox = Object.create(vm.user_pillbox.box7); break;
                    case 8: vm.modalpillbox = Object.create(vm.user_pillbox.box8); break;
                }
                
                console.log("B4, modalpillbox: ", vm.modalpillbox);
                
                $ionicModal.fromTemplateUrl('templates/modal_pillbox.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function(modal) {
                    $scope.modal = modal;
                    $scope.modal.show();
                });

            }


            vm.cancelModal = function() {
                $scope.modal.remove();
            };

            vm.savePillBox = function() {
                $scope.modal.remove();

                var tmpPillBox = {};

                switch (vm.modalpillboxNum)
                {
                    case 1: tmpPillBox = vm.user_pillbox.box1; break;
                    case 2: tmpPillBox = vm.user_pillbox.box2; break;
                    case 3: tmpPillBox = vm.user_pillbox.box3; break;
                    case 4: tmpPillBox = vm.user_pillbox.box4; break;
                    case 5: tmpPillBox = vm.user_pillbox.box5; break;
                    case 6: tmpPillBox = vm.user_pillbox.box6; break;
                    case 7: tmpPillBox = vm.user_pillbox.box7; break;
                    case 8: tmpPillBox = vm.user_pillbox.box8; break;
                }

                tmpPillBox.pill_name = vm.modalpillbox.pill_name;
                tmpPillBox.freq = vm.modalpillbox.freq;
                tmpPillBox.dose = vm.modalpillbox.dose;
                tmpPillBox.left = vm.modalpillbox.left;
                tmpPillBox.admin = $rootScope.loggedUser.id;
                
            };
            

            // Simulated epillbox medication regimen information
            vm.epillbox_index = ['S1234567Z', 'S1974932A', 'S2091182C', 'S2191182C', 'S2291182C'];

            vm.epillbox_table = [
                {   'id': 'S1234567Z', 
                    'box1': {'pill_name': 'Sitagliptin',            'freq': '4', 'dose': '1', 'adhere': 'NG', 'left': '10', 'admin': 'S1234567Z'},
                    'box2': {'pill_name': 'Pregabalin',             'freq': '3', 'dose': '1', 'adhere': 'NG', 'left': '8',  'admin': 'S1234567Z'},
                    'box3': {'pill_name': 'Insulin glargine',       'freq': '3', 'dose': '1', 'adhere': 'NG', 'left': '8',  'admin': 'S1234567Z'},
                    'box4': {'pill_name': 'Rosuvastatin calcium',   'freq': '2', 'dose': '2', 'adhere': 'GD', 'left': '10', 'admin': 'S1234567Z'},
                    'box5': {'pill_name': 'Tadalafil',              'freq': '2', 'dose': '1', 'adhere': 'GD', 'left': '6',  'admin': 'S1234567Z'},
                    'box6': {'pill_name': 'Colesevelam Hcl',        'freq': '2', 'dose': '2', 'adhere': 'NG', 'left': '8',  'admin': 'S1234567Z'},
                    'box7': {'pill_name': 'Carvedilol',             'freq': '3', 'dose': '1', 'adhere': 'NG', 'left': '8',  'admin': 'S1234567Z'},
                    'box8': {'pill_name': 'Estradiol',              'freq': '3', 'dose': '1', 'adhere': 'GD', 'left': '6',  'admin': 'S1234567Z'}

                },
            
                {   'id': 'S1974932A',  
                    'box1': {'pill_name': 'Carvedilol',             'freq': '3', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S1974932A'},
                    'box2': {'pill_name': 'Colesevelam Hcl',        'freq': '2', 'dose': '2', 'adhere': 'GD', 'left': '6',  'admin': 'S1974932A'},
                    'box3': {'pill_name': 'Tadalafil',              'freq': '3', 'dose': '2', 'adhere': 'GD', 'left': '6',  'admin': 'S1974932A'},
                    'box4': {'pill_name': 'Rosuvastatin calcium',   'freq': '4', 'dose': '1', 'adhere': 'GD', 'left': '10', 'admin': 'S1974932A'},
                    'box5': {'pill_name': 'Insulin glargine',       'freq': '3', 'dose': '1', 'adhere': 'GD', 'left': '6',  'admin': 'S1974932A'},
                    'box6': {'pill_name': 'Pregabalin',             'freq': '2', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S1974932A'},
                    'box7': {'pill_name': 'Estradiol',              'freq': '1', 'dose': '2', 'adhere': 'GD', 'left': '6',  'admin': 'S1974932A'},
                    'box8': {'pill_name': 'Sitagliptin',            'freq': '2', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S1974932A'}

                },

                {   'id': 'S2091182C',  
                    'box1': {'pill_name': 'Insulin glargine',       'freq': '3', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S2091182C'},
                    'box2': {'pill_name': 'Pregabalin',             'freq': '4', 'dose': '1', 'adhere': 'GD', 'left': '10', 'admin': 'S2091182C'},
                    'box3': {'pill_name': 'Estradiol',              'freq': '1', 'dose': '2', 'adhere': 'GD', 'left': '6',  'admin': 'S2091182C'},
                    'box4': {'pill_name': 'Sitagliptin',            'freq': '3', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S2091182C'},
                    'box5': {'pill_name': 'Carvedilol',             'freq': '3', 'dose': '1', 'adhere': 'GD', 'left': '5',  'admin': 'S2091182C'},
                    'box6': {'pill_name': 'Colesevelam Hcl',        'freq': '3', 'dose': '2', 'adhere': 'GD', 'left': '9',  'admin': 'S2091182C'},
                    'box7': {'pill_name': 'Tadalafil',              'freq': '4', 'dose': '1', 'adhere': 'GD', 'left': '6',  'admin': 'S2091182C'},
                    'box8': {'pill_name': 'Rosuvastatin calcium',   'freq': '2', 'dose': '2', 'adhere': 'GD', 'left': '8',  'admin': 'S2091182C'}

                },

                {   'id': 'S2191182C',  
                    'box1': {'pill_name': 'Rosuvastatin calcium',   'freq': '3', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S2191182C'},
                    'box2': {'pill_name': 'Estradiol',              'freq': '2', 'dose': '2', 'adhere': 'GD', 'left': '6',  'admin': 'S2191182C'},
                    'box3': {'pill_name': 'Pregabalin',             'freq': '4', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S2191182C'},
                    'box4': {'pill_name': 'Carvedilol',             'freq': '1', 'dose': '2', 'adhere': 'GD', 'left': '5',  'admin': 'S2191182C'},
                    'box5': {'pill_name': 'Sitagliptin',            'freq': '2', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S2191182C'},
                    'box6': {'pill_name': 'Insulin glargine',       'freq': '3', 'dose': '1', 'adhere': 'GD', 'left': '10', 'admin': 'S2191182C'},
                    'box7': {'pill_name': 'Rosuvastatin calcium',   'freq': '4', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S2191182C'},
                    'box8': {'pill_name': 'Tadalafil',              'freq': '3', 'dose': '2', 'adhere': 'GD', 'left': '6',  'admin': 'S2191182C}'}

                },

                {   'id': 'S2291182C',  
                    'box1': {'pill_name': 'Sitagliptin',            'freq': '3', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S2291182C'},
                    'box2': {'pill_name': 'Insulin glargine',       'freq': '4', 'dose': '2', 'adhere': 'GD', 'left': '6',  'admin': 'S2291182C'},
                    'box3': {'pill_name': 'Rosuvastatin calcium',   'freq': '2', 'dose': '2', 'adhere': 'NG', 'left': '8',  'admin': 'S2291182C'},
                    'box4': {'pill_name': 'Tadalafil',              'freq': '3', 'dose': '1', 'adhere': 'NG', 'left': '10', 'admin': 'S2291182C'},
                    'box5': {'pill_name': 'Rosuvastatin calcium',   'freq': '1', 'dose': '2', 'adhere': 'GD', 'left': '8',  'admin': 'S2291182C'},
                    'box6': {'pill_name': 'Estradiol',              'freq': '2', 'dose': '1', 'adhere': 'GD', 'left': '6',  'admin': 'S2291182C'},
                    'box7': {'pill_name': 'Pregabalin',             'freq': '3', 'dose': '1', 'adhere': 'NG', 'left': '6',  'admin': 'S2291182C'},
                    'box8': {'pill_name': 'Carvedilol',             'freq': '4', 'dose': '1', 'adhere': 'GD', 'left': '8',  'admin': 'S2291182C'}

                }   
            ];

            // console.log('epillbox_index', vm.epillbox_index);
            // console.log('epillbox_table', vm.epillbox_table);

            // ************************ end of data table
        }

        function PatientListCtrl($rootScope, $scope, $ionicScrollDelegate, filterFilter, $location, 
                    $anchorScroll, $state, $ionicHistory, $ionicPopup, PatientsAPI) {

            var vm = this;
            
            vm.mainselect ='NRIC (ID)';
            vm.showAdhere = "";
            vm.search = "";

            // patient.adhere --- level of adherence 

            vm.patientList = [];

            console.log("$rootScope.loggedUser: ", $rootScope.loggedUser);

            vm.supervisoraccess = ($rootScope.loggedUser.role == "SUPERVISOR");

            if (vm.supervisoraccess) {

                // SUPERVISOR view all Patients

                PatientsAPI.list()
                    .then(function(data) {
                        vm.patientList = data;
                        console.log("Get All Patients: ", vm.patientList);
                    })
                    .catch(function(err) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Err P1: Database Error!',
                            template: 'Logging Off now'
                        });
                        $state.go('login');
                    })
            }
            else {

                // Non SUPERVISOR only view patients under their care

                PatientsAPI.searchByNurseID($rootScope.loggedUser.id)
                    .then(function(data) {
                        vm.patientList = data;
                        console.log("Get All Patients by NurseID: ", vm.patientList);
                    })
                    .catch(function(err) {
                        var alertPopup = $ionicPopup.alert({
                            title: 'Err P2: Database Error!',
                            template: 'Logging Off now'
                        });
                        $state.go('login');
                    })
            }

            vm.scrollTop = function() {
                $ionicScrollDelegate.scrollTop();
            };
  
            vm.scrollBottom = function() {
                $ionicScrollDelegate.scrollBottom();
            };

            vm.clearSearch = function() {
                vm.search = '';
            };

            vm.viewpatient = function(id) {

                console.log("view patient clicked");

                $state.go("tab.patientsingle", {patientId: id});
            }

        }

})();
