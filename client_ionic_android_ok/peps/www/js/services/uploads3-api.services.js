/*
    patients-api.service.js
 */
(function () {
    angular.module("pepsApp")
        .service("UploadS3API", ["$http", "$q", "API_ENDPOINT", "Upload", UploadS3API]);

    function UploadS3API($http, $q, API_ENDPOINT, Upload) {
        var vm = this;

        vm.statusS3 = {
                message : "",
                code : 0
            };


        vm.uploadNurse = function (nurseId, imgFile) {
            var defer = $q.defer();

            console.log("upload: nurseID = ", nurseId);
            console.log("upload: imgFile = ", imgFile);

            Upload.upload({
                url: API_ENDPOINT.url+"/uploads3/nurses/"+nurseId,
                data: {
                    "img-file": imgFile,
                }
            }).then(function(response){
                vm.statusS3.message = response.data;
                vm.statusS3.code = 202;
                console.log("uploadNurse: message: ",vm.statusS3.message);
                defer.resolve(response.data);
            }).catch(function(err){
                console.log(err);
                vm.statusS3.message = "Error";
                vm.statusS3.code = 500;
                defer.reject(err);
            });
            return defer.promise;
        };

        vm.uploadPatient = function (patientId, imgFile) {
            Upload.upload({
                url: API_ENDPOINT.url+"/uploads3/patients/"+patientId,
                data: {
                    "img-file": imgFile,
                }
            }).then(function(response){
                vm.statusS3.message = response.data;
                vm.statusS3.code = 202;
                console.log(JSON.stringify(vm.statusS3.message));
                return 
            }).catch(function(err){
                console.log(err);
                vm.statusS3.message = "Error";
                vm.statusS3.code = 500;
            })
        };

        
    }
})();
