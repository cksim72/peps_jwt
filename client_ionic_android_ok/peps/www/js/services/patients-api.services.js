/*
    patients-api.service.js
 */
(function () {
    angular.module("pepsApp")
        .service("PatientsAPI", ["$http", "$q", "API_ENDPOINT", PatientsAPI]);

    function PatientsAPI($http, $q, API_ENDPOINT) {
        var vm = this;

        vm.list = function (){
            var defer = $q.defer();
            console.log("Patients List");
            $http.get(API_ENDPOINT.url + "/patients")
                .then(function(result) {
                    defer.resolve(result.data);
                })
                .catch(function(error) {
                    defer.reject(error);
            });
            return defer.promise;
        };

        vm.search = function (patientId) {
            var defer = $q.defer();
            $http.get(API_ENDPOINT.url + "/patients/" + patientId)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };

        vm.searchByNurseID = function (nurseId) {
            var defer = $q.defer();
            $http.get(API_ENDPOINT.url + "/patients/nurse/" + nurseId)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };
        
        vm.save = function (patient) {
            var defer = $q.defer();
            
            console.log("Patient save: ", patient);

            $http.put(API_ENDPOINT.url + "/patients/" + patient.id, patient)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };

        vm.remove = function (patient) {
            var defer = $q.defer();
            $http.delete(API_ENDPOINT.url + "/patients/" + patient.id)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };

        vm.insert = function (patient) {
            var defer = $q.defer();
            $http.post(API_ENDPOINT.url + "/patients/", patient)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
    }
})();
