/*
    nurses-api.service.js
 */
(function () {
    angular.module("pepsApp")
        .service("NursesAPI", ["$http", "$q", "API_ENDPOINT", NursesAPI]);

    function NursesAPI($http, $q, API_ENDPOINT) {
        var vm = this;

        vm.list = function (callback){
            var defer = $q.defer();
            $http.get(API_ENDPOINT.url + "/nurses")
                .then(function(result){
                    console.log("Nurses getAll result: ", result.data)
                    defer.resolve(result.data);
                }).catch(function(error){
                defer.reject(error);
            });
            return defer.promise;
        };

        vm.search = function (nurseID) {
            var defer = $q.defer();
            $http.get(API_ENDPOINT.url + "/nurses/" + nurseID)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };
        
        vm.save = function (nurse) {
            var defer = $q.defer();
            $http.put(API_ENDPOINT.url + "/nurses/" + nurse.id, nurse)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };

        vm.remove = function (nurse) {
            var defer = $q.defer();
            $http.delete(API_ENDPOINT.url + "/nurses/" + nurse.id)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
    }
})();
