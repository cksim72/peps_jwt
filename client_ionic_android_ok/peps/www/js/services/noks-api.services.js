/*
    noks-api.service.js
 */
(function () {
    angular.module("pepsApp")
        .service("NOKsAPI", ["$http", "$q", "API_ENDPOINT", NOKsAPI]);

    function NOKsAPI($http, $q, API_ENDPOINT) {
        var vm = this;

        vm.list = function (){
            var defer = $q.defer();
            console.log("NOKs List");
            $http.get(API_ENDPOINT.url + "/noks")
                .then(function(result) {
                    defer.resolve(result.data);
                })
                .catch(function(error) {
                    defer.reject(error);
            });
            return defer.promise;
        };

        vm.search = function (nokId) {
            var defer = $q.defer();
            $http.get(API_ENDPOINT.url + "/noks/" + nokId)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };

        vm.searchByPatientID = function (nokId) {
            var defer = $q.defer();
            $http.get(API_ENDPOINT.url + "/noks/patient/" + nokId)
                .then(function (result) {
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };
        
        vm.save = function (nok) {
            var defer = $q.defer();
            
            console.log("NOK save: ", nok);

            $http.put(API_ENDPOINT.url + "/noks/" + nok.id, nok)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };

        vm.remove = function (nok) {
            var defer = $q.defer();
            $http.delete(API_ENDPOINT.url + "/noks/" + nok.id)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };

        vm.insert = function (nok) {
            var defer = $q.defer();
            $http.post(API_ENDPOINT.url + "/noks/", nok)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        };
    }
})();
