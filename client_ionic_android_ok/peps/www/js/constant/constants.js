angular.module('pepsApp')
 
.constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated'
})
 
.constant('API_ENDPOINT', {
  // url: 'http://127.0.0.1:8100/api'
  //  For a simulator use: 
  // url: 'http://127.0.0.1:8080/api'
   url: 'http://ec2-54-179-171-126.ap-southeast-1.compute.amazonaws.com:8080/api'
});
