// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('pepsApp', ['ionic'])

.run(function($ionicPlatform) {

  $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.views.maxCache(0);
    // $ionicConfigProvider.backButton.text('Go Back').icon('ion-chevron-left');
    $ionicConfigProvider.navBar.alignTitle("center"); //Places them at the center for all OS
    $ionicConfigProvider.tabs.position("bottom"); //Places them at the bottom for all OS
    $ionicConfigProvider.tabs.style("standard"); //Makes them all look the same across all OS

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
    $stateProvider

    .state('login', {
        cache: false,
        url: "/login",
        templateUrl: "templates/login.html",
        controller: 'LoginCtrl as ctrl'
    })
    // setup an abstract state for the tabs directive
    .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
    })

    // Each tab has its own nav history stack:

    .state('tab.patientlist', {
        cache: false,
        url: '/patients',
        views: {
                'tab-patient': {
                templateUrl: 'templates/patientlist.html',
                controller: 'PatientListCtrl as ctrl'
                }
            }
    })

    .state('tab.patientsingle', {
        cache: false,
        url: "/patients/:patientId",
        views: {
            'tab-patient' :{
                templateUrl: "templates/patientprofile.html",
                controller: 'PatientCtrl as ctrl'
            }
        }
    })

    .state('tab.nurselist', {
        cache: false,
        url: '/nurses',
        views: {
            'tab-nurse': {
            templateUrl: 'templates/nurselist.html',
            controller: 'NurseListCtrl as ctrl'
            }
        }
    })

    .state('tab.nursesingle', {
        cache: false,
        url: "/nurses/:nurseId",
        views: {
            'tab-nurse' :{
                templateUrl: "templates/nurseprofile.html",
                controller: 'NurseCtrl as ctrl'
            }
        }
    })
    .state('tab.account', {
        cache: false,
        url: '/account',
        views: {
        'tab-account': {
            templateUrl: 'templates/account.html',
            controller: 'AccountCtrl as ctrl'
            }
        }
    })

    // .state('tab.myprofile', {
    //     cache: false,
    //     url: '/myprofile',
    //     views: {
    //         'tab-account': {
    //             templateUrl: 'templates/myprofile.html',
    //             controller: 'AccountCtrl as ctrl'
    //         }
    //     }
    // })

    .state('tab.epillboxes', {
        url: '/epillboxes',
        views: {
            'tab-epillbox': {
            templateUrl: 'templates/epillboxes.html',
            controller: 'EPillBoxListCtrl as ctrl'
            }
        }
    })

    // .state('tab.chats', {
    //     url: '/chats',
    //     views: {
    //         'tab-chats': {
    //             templateUrl: 'templates/tab-chats.html',
    //             controller: 'ChatsCtrl'
    //         }
    //     }
    // })

    // .state('tab.chat-detail', {
    //     url: '/chats/:chatId',
    //     views: {
    //         'tab-chats': {
    //             templateUrl: 'templates/chat-detail.html',
    //             controller: 'ChatDetailCtrl'
    //         }
    //     }
    // })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/login');

})
.run(function ($rootScope, $state, $timeout, AuthService, AUTH_EVENTS) {

  $rootScope.$on('$stateChangeStart', function (event,next, nextParams, fromState) {
    if (!AuthService.isAuthenticated()) {
      console.log("next.name", next.name);
      if (next.name !== 'login') {
        event.preventDefault();
        $state.go('login');
      }
    }
  });
});