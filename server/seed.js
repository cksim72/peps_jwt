// Create initial data set

var bcrypt   = require('bcryptjs');
var config = require("./config");
var database = require("./database");

var Logins = database.Logins;
var Patients = database.Patients;
var NextOfKins = database.NextOfKins;
var Nurses = database.Nurses;
var Teams = database.Teams;
var PillDispensers = database.PillDispensers;
var Doctors = database.Doctors;


module.exports = function () {
    if (config.seed) {
        var hashpassword = bcrypt.hashSync("Password@123", bcrypt.genSaltSync(8), null);

        var nArray = [];
        var lArray = [];
        var pArray = [];

        Nurses
            .create({
                id: "S8012056Z",
                first_name: "Mary",
                last_name: "Lim",
                gender: "F",
                position: "JUNIOR",
                address: "Singapore",
                birth_date: "1980-03-22T04:27:46.000Z",
                contact: "91827365",
                email: "marylim@gmail.com",
                created_by_id: "S2175302A"
            })
            .then(function (result) {
                console.log("Nurses created\n");
            }).catch(function (err) {
                console.log("Nurses create error", err)
            })

        Logins
            .create({
                    id: "S8012056Z",
                    password: hashpassword,
                    created_by_id: "S2175302A"
                })
            .then(function (result) {
                console.log("Logins created\n");
            }).catch(function (err) {
                console.log("Logins create error", err)
            })

        Doctors
            .create({
                id: "S7012056Z",
                first_name: "John",
                last_name: "Lim",
                gender: "M",
                company: "HOSPITAL",
                address: "Singapore",
                birth_date: "1970-03-22T04:27:46.000Z",
                contact: "81827365",
                email: "johnlim@gmail.com",
                created_by_id: "S2175302A"
            })
            .then(function (result) {
                console.log("Doctor created\n");
                Patients
                    .create({
                            id: "S2191182C",
                            first_name: "Henry",
                            last_name: "Chia",
                            gender: "M",
                            address: "Singapore",
                            birth_date: "1945-04-13T04:27:46.000Z",
                            contact: "91827359",
                            email: "henrychia@gmail.com",
                            nurse_id: "S8012056Z",
                            doctor_id: "S7012056Z",
                            created_by_id: "S2175302A"
                        })
            }).catch(function (err) {
                console.log("Doctor create error", err)
            })
        console.log(pArray);

    }
};


                    
