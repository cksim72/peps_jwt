// Model for Pillpresso dispenser table

var Sequelize = require("sequelize");

module.exports = function(database) {
    return database.define('pilldispensers', {
        serialno: {
            type: Sequelize.STRING(32),
            allowNull: false,
            primaryKey: true
        },
        dateOfManufacture: {
            type: Sequelize.DATE,
            allowNull: false
        },
        model: {
            type: Sequelize.STRING(32),
            allowNull: false
        },
        patient_id: {
            type: Sequelize.STRING(16),
            allowNull: false,
            primaryKey: true,
            references : {
                model: 'patients',
                key: 'id'
            }
        },
        created_by_id: {
            type: Sequelize.STRING(16),
            allowNull: false
        }
    }, {
        freezeTableName: true,
        tableName: 'pilldispensers',
        timestamps: true
    });
};