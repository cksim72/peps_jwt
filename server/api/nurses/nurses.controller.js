var Nurses = require("../../database").Nurses;
// var bcrypt   = require('bcryptjs');
var config = require("../../config");
// var api_key = config.mailgun_key;
// var domain = config.mailgun_domain;
// var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
// var mailcomposer = require('mailcomposer');

exports.get = function (req, res) {

    console.log("Nurse ID", req.params.id);

    Nurses
        .findById(req.params.id)
        .then(function (user) {
            // console.log("Nurse found: ", user);

            if (!user) {
                handler404(res);
            }

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

// exports.register = function(req, res) {
//     if(!req.body.password === req.body.confirmpassword) {
//         return res.status(500).json({
//             err: err
//         });
//     }
//     console.log(req.body.password);
//     var hashpassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
//     User.findOrCreate({where: {email: req.body.username,},defaults: {
//         username: req.body.username,
//         email: req.body.username,
//         password: hashpassword,
//         firstName: req.body.firstName,
//         lastName: req.body.lastName}})
//         .spread(function(user, created) {
//             if(created){
//                 user.password = "";
//                 res.status(200);
//                 var data = {
//                     from: config.register_email.from,
//                     to: user.email,
//                     subject: config.register_email.subject,
//                     text: config.register_email.email_text
//                 };
                
//                 mailgun.messages().send(data, function (error, body) {
//                     console.log(body);
//                 });
//                 returnResults(user,res);
//             }else{
//                 user.password = "";
//                 handleErr(res);
//             }
//         }).error(function(error){
//             handleErr(res, err);
//     });
// };

exports.list = function (req, res) {
    Nurses
        .findAll()
        .then(function (users) {
            res.json(users);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.create = function (req, res) {
    Nurses
        .create(req.body)
        .then(function (user) {

            console.log("Nurse record created");
            
            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.update = function(req,res) {
    Nurses
        .find({
            where: {
                id: req.params.id
            }
        })
        .then(function(result){
            result.updateAttributes({
                id: req.body.id,
                birth_date: req.body.birth_date,
                gender: req.body.gender,
                last_name: req.body.last_name,
                first_name: req.body.first_name,
                email: req.body.email,
                address: req.body.address,
                contact: req.body.contact,
                created_by_id: req.body.created_by_id

            }).then(function (){
                console.log("update done");
                res.status(200).end();
            }).catch(function (){
                console.log("update failed");
                res
                    .status(500)
                    .json({error: true, errorText: "Update Failed"})
            });
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true, errorText: "Record not found"})
        });

};

exports.remove = function (req, res) {

    Nurses
        .destroy({
            where: {id: req.params.id}
        })
        .then(function (user) {
            if (!user) {
                handler404(res);
            }

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.profile = function (req, res) {
    Nurses.findOne({where: {id: req.user.id}})
        .then(function(result) {
            res.json(result);
        }).catch(function (err) {
        console.error(err);
        handleErr(res, err);
    });
};


function handleErr(res) {
    handleErr(res, null);
}


function handleErr(res, err) {
    console.log(err);
    res
        .status(500)
        .json({
            error: true
        });
}

function handler404(res) {
    res
        .status(404)
        .json({message: "User not found!"});
}

function returnResults(results, res) {
    res.send(results);
}
