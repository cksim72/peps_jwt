//
// next-of-kins.controller.js
//

var NOKs = require("../../database").NextOfKins;
var config = require("../../config");


exports.get = function (req, res) {
    NOKs
        .findById(req.params.id)
        .then(function (user) {

            if (!user) {
                handler404(res);
            }

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.list = function (req, res) {
    NOKs
        .findAll()
        .then(function (nok) {
            res.json(nok);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.create = function (req, res) {
    NOKs
        .create(req.body)
        .then(function (user) {

            console.log("NOK record created");

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.update = function (req, res) {

    NOKs
        .find({
            where: {
                id: req.params.id
            }
        })
        .then(function(result){
            result.updateAttributes({
                id: req.body.id,
                gender: req.body.gender,
                last_name: req.body.last_name,
                first_name: req.body.first_name,
                email: req.body.email,
                address: req.body.address,
                contact: req.body.contact,
                patient_id: req.body.patient_id,
                relationship: req.body.relationship,
                created_by_id: req.body.created_by_id

            }).then(function (){
                console.log("NOK update done");
                res.status(200).end();
            }).catch(function (){
                console.log("NOK update failed");
                res
                    .status(500)
                    .json({error: true, errorText: "Update Failed"})
            });
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true, errorText: "Record not found"})
        });

};

exports.remove = function (req, res) {
    NOKs
        .destroy({
            where: {id: req.params.id}
        })
        .then(function (user) {

            console.log("NOK record deleted");

            if (!user) {
                handler404(res);
            }

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.listByPatientID = function(req,res) {
    console.log("req.params", req.params);
    NOKs
        .findAll({
            where: {
                patient_id: req.params.patient_id
            }
        })
        .then(function (noks) {
            res.json(noks);
        })
        .catch(function (err) {
            handleErr(res, err);
        });

};


function handleErr(res) {
    handleErr(res, null);
}


function handleErr(res, err) {
    console.log(err);
    res
        .status(500)
        .json({
            error: true
        });
}

function handler404(res) {
    res
        .status(404)
        .json({message: "Patient not found!"});
}

function returnResults(results, res) {
    res.send(results);
}
