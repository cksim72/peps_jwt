var Patients = require("../../database").Patients;
// var bcrypt   = require('bcryptjs');
var config = require("../../config");
// var api_key = config.mailgun_key;
// var domain = config.mailgun_domain;
// var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
// var mailcomposer = require('mailcomposer');

exports.get = function (req, res) {
    Patients
        .findById(req.params.id)
        .then(function (user) {

            if (!user) {
                handler404(res);
            }

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.list = function (req, res) {
    Patients
        .findAll()
        .then(function (patients) {
            res.json(patients);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.create = function (req, res) {
    Patients
        .create(req.body)
        .then(function (user) {

            console.log("Patient record created");

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.update = function (req, res) {

    Patients
        .find({
            where: {
                id: req.params.id
            }
        })
        .then(function(result){
            result.updateAttributes({
                id: req.body.id,
                birth_date: req.body.birth_date,
                gender: req.body.gender,
                last_name: req.body.last_name,
                first_name: req.body.first_name,
                email: req.body.email,
                address: req.body.address,
                contact: req.body.contact,
                nurse_id: req.body.nurse_id,
                doctor_id: req.body.doctor_id,
                created_by_id: req.body.created_by_id,
                adhere: req.body.adhere

            }).then(function (){
                console.log("update done");
                res.status(200).end();
            }).catch(function (){
                console.log("update failed");
                res
                    .status(500)
                    .json({error: true, errorText: "Update Failed"})
            });
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true, errorText: "Record not found"})
        });

};

exports.remove = function (req, res) {
    Patients
        .destroy({
            where: {id: req.params.id}
        })
        .then(function (user) {

            console.log("Patient record deleted");

            if (!user) {
                handler404(res);
            }

            res.json(user);
        })
        .catch(function (err) {
            handleErr(res, err);
        });
};

exports.listByNurseID = function(req,res) {
    console.log("req.params", req.params);
    Patients
        .findAll({
            where: {
                nurse_id: req.params.nurse_id
            }
        })
        .then(function (patients) {
            res.json(patients);
        })
        .catch(function (err) {
            handleErr(res, err);
        });

};


function handleErr(res) {
    handleErr(res, null);
}


function handleErr(res, err) {
    console.log(err);
    res
        .status(500)
        .json({
            error: true
        });
}

function handler404(res) {
    res
        .status(404)
        .json({message: "Patient not found!"});
}

function returnResults(results, res) {
    res.send(results);
}
