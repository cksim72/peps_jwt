// aws-s3.js
var path = require("path");
var AWS = require('aws-sdk');
var multer = require('multer');     // interpret as binary
var multerS3 = require('multer-s3');


AWS.config.region = 'ap-southeast-1';
console.log("AWS Bucket init ....");

var s3Bucket = new AWS.S3({
    // params: {
    //     Bucket: "nus-iss-stackup"
    // }
});

var patientUploadS3 = multer({
    storage: multerS3({
        s3: s3Bucket,     
        bucket: "nus-iss-stackup",
        metadata: function(req, file, cb){
            cb(null, {fieldName: file.fieldname});
        },
        key: function(req, file, cb){
            // cb(null, Date.now() +'-'+ file.originalname);           // key: for connection to S3
            cb(null, 'patients/'+ req.params.id+ path.extname(file.originalname));  
        }
    })
})

var nurseUploadS3 = multer({
    storage: multerS3({
        s3: s3Bucket,
        bucket: "nus-iss-stackup",
        metadata: function(req, file, cb){
            cb(null, {fieldName: file.fieldname});
        },
        key: function(req, file, cb){
            // cb(null, Date.now() +'-'+ file.originalname);           // key: for connection to S3
            cb(null, 'nurses/'+ req.params.id + path.extname(file.originalname));
        }
    })
})

module.exports = function(app, passport){

    app.post("/api/uploads3/patients/:id", passport.authenticate('jwt', { session: false}), patientUploadS3.single("img-file"),function(req, res){
        console.log("Patient: Upload S3...");
        console.log(JSON.stringify(req));
        res.send("Successfully uploaded. File: "+ req.file);
    });

    app.post("/api/uploads3/nurses/:id", passport.authenticate('jwt', { session: false}), nurseUploadS3.single("img-file"), function(req, res){
        console.log("Nurse: Upload S3...");
        console.log(JSON.stringify(req.file));
        res.send("Successfully uploaded. File: "+ req.file);
    });
}
