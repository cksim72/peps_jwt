'use strict';

// var AWSController = require("./api/aws/aws.controller");

var PatientsController = require("./api/patients/patients.controller");
var NursesController = require("./api/nurses/nurses.controller");
// var TeamsController = require("./api/teams/teams.controller");
// var PillDispensersController = require("./api/pilldispensers/pilldispensers.controller");
var NOKsController = require("./api/noks/next-of-kins.controller");
// var LoginsController = require("./api/logins/logins.controller");


var express = require("express");
var config = require("./config");

var jwt = require('jsonwebtoken');

const API_PILLDISPENSERS_URI = "/api/pilldispensers";
const API_NEXT_OF_KINS_URI = "/api/noks";
const API_PATIENTS_URI = "/api/patients";
const API_NURSES_URI = "/api/nurses";
const API_TEAMS_URI = "/api/teams";
const API_DOCTORS_URI = "/api/doctors";

// const API_AWS_URI = "/api/aws";

const TEAM_LEADER_HOME_PAGE = "/#!/supervisor";
const NURSE_HOME_PAGE = "/#!/profile";
const SIGNIN_PAGE = "/#!/login";


module.exports = function (app, passport) {

/*
    // Teams API
    app.get(API_TEAMS_URI, TeamsController.list);                               //List All

    // Pill Dispensers API
    app.get(API_PILLDISPENSERS_URI, PillDispensersController.list);             //List All

    // NOKs API
    app.get(API_NEXT_OF_KINS_URI, NOKsController.list);                         //List All

    //  Logins API
    app.get(API_LOGINS_URI, LoginsController.list);                             //List All
    

*/    
    // Static path
    // app.use(express.static(__dirname + "/../client/"));

    // app.post("/change-password", isAuthenticated, UserController.changePasswd);
    // app.get("/api/user/get-profile-token", UserController.profileToken);
    // app.post("/api/user/change-passwordToken", UserController.changePasswdToken);

    //unprotected end point
    // app.post('/register', UserController.register);
    // app.post("/reset-password", UserController.resetPasswd);
    
    // app.get('/home', isAuthenticated, function(req, res) {
    //     res.redirect('..' + TEAM_LEADER_HOME_PAGE);
    // });

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        if (req.method === 'OPTIONS') {
            res.end();
            } else {
            next();
        }
    });

    app.post("/api/authenticate", passport.authenticate('jwt', { session: false}) ,function(req, res, next) {
        
        if (req.user) {
            console.log("req.user: ", req.user);
            res.status(200);
            res.send(req.user);
        }
        else res.status(401).send("Invalid Credentials");
        
    });

    // Patients API
    app.get(API_PATIENTS_URI, passport.authenticate('jwt', { session: false}), PatientsController.list);            // List All
    app.post(API_PATIENTS_URI, passport.authenticate('jwt', { session: false}), PatientsController.create);         // Create 
    app.get("/api/patients/:id", passport.authenticate('jwt', { session: false}), PatientsController.get);          // Retrieve by ID
    app.put("/api/patients/:id", passport.authenticate('jwt', { session: false}), PatientsController.update);       // Update
    app.get("/api/patients/nurse/:nurse_id", passport.authenticate('jwt', { session: false}), PatientsController.listByNurseID);    // List by Nurse ID
    app.delete("/api/patients/:id", passport.authenticate('jwt', { session: false}), PatientsController.remove);    // Delete

    // Nurses API
    app.get(API_NURSES_URI, passport.authenticate('jwt', { session: false}), NursesController.list);                // List All
    app.post(API_NURSES_URI, passport.authenticate('jwt', { session: false}), NursesController.create);             // Create 
    app.get("/api/nurses/:id", passport.authenticate('jwt', { session: false}), NursesController.get);              // Retrieve by ID
    app.put("/api/nurses/:id", passport.authenticate('jwt', { session: false}), NursesController.update);           // Update
    app.delete("/api/nurses/:id", passport.authenticate('jwt', { session: false}), NursesController.remove);        // Delete

    // NOKs API
    app.get(API_PATIENTS_URI, passport.authenticate('jwt', { session: false}), NOKsController.list);                // List All
    app.post(API_PATIENTS_URI, passport.authenticate('jwt', { session: false}), NOKsController.create);             // Create 
    app.get("/api/noks/:id", passport.authenticate('jwt', { session: false}), NOKsController.get);                  // Retrieve by ID
    app.put("/api/noks/:id", passport.authenticate('jwt', { session: false}), NOKsController.update);               // Update
    app.get("/api/noks/patient/:patient_id", passport.authenticate('jwt', { session: false}), NOKsController.listByPatientID);    // List by Patient ID
    app.delete("/api/noks/:id", passport.authenticate('jwt', { session: false}), NOKsController.remove);            // Delete


    app.post("/api/login", function(req, res, next) {

        // console.log("req.body", req.body);

        passport.authenticate("local", function (err, user, info) {
            if (err) {
                return res.status(401).send({
                    success: false,
                    error: err
                });
            }

            if (!user) {
                return res.status(401).send({
                    success: false,
                    error: info
                });
            }

            console.log("local authenticate OK");

            const token = jwt.sign({
                                    id: user.id
                                }, config.jwtsecret, {
                                    expiresIn: 216000, // In seconds = 2.5 days
                                    audience: 'peps.com',
                                    issuer: 'pepsApp'
                                });

            return res.json({
                                success: true, 
                                role: user.role,
                                token: 'JWT '+token});

        })(req, res, next);
    });

    // app.get('/protected/', isAuthenticated, function(req, res) {
    //     if(req.user == null){
    //         res.redirect(SIGNIN_PAGE);
    //     }
    //     // res.redirect('..' + TEAM_LEADER_HOME_PAGE);
    // });

    // app.post("/login", passport.authenticate("local", {
    //     successRedirect: NURSE_HOME_PAGE,
    //     failureRedirect: "/",
    //     failureFlash : true
    // }));

    // app.get("/status/user", function (req, res) {
    //     var status = "";
    //     if(req.user) {
    //         status = req.user.id;
    //     }
    //     console.info("status of the user --> " + status);
    //     res.send(status).end();
    // });

    // app.get("/logout", function(req, res) {
    //     req.logout();             // clears the passport session
    //     req.session.destroy();    // destroys all session related data
    //     res.send(req.user).end();
    // });

    // function isAuthenticated(req, res, next) {
    //     if (req.isAuthenticated())
    //         return next();
    //     res.redirect(SIGNIN_PAGE);
    // }

    
    app.use(function(req, res, next){
        // if(req.user == null){
        //     res.redirect(SIGNIN_PAGE);
        // }
        next();
    });

};
